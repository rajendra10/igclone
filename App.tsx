import 'react-native-gesture-handler';

import React from 'react';
import {View, SafeAreaView, Text} from 'react-native';

import RoutedScreens from './src/reactRouter';
import {Provider} from 'react-redux';

import store from './src/redux';

const App = () => {
	return (
		<>
			<Provider store = {store} >
				<RoutedScreens/>
			</Provider>
		</>
	);
};

export default App;
