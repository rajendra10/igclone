// In App.js in a new project

import * as React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useSelector} from 'react-redux';
import BottomTabs from './BottomTabs';
import SharedScreens from './SharedScreens';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

import {LoginScreen} from '../screens';

export default () => {
	const authState = useSelector(store => store.auth);
	return (
		<NavigationContainer>
			<Stack.Navigator
				screenOptions={{
					headerShown: false,
				}}>
				{authState.user ? (
					<>
						<Stack.Screen name="BottomTabs" component={BottomTabs} />
						<Stack.Screen name="Shared" component={SharedScreens} />
					</>
				) : (
					<Stack.Screen name="Login" component={LoginScreen} />
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
};
