import * as React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';


const Stack = createStackNavigator();
import {StoriesScreen, DirectsScreen, ChatScreen} from '../screens';
import {Posts as PostsScreen} from '../components';

 export default ()=>{
	return(
		<Stack.Navigator
			screenOptions= {{
					headerShown : false
				}}
		>
			<Stack.Screen name="Stories" component={StoriesScreen} />
			<Stack.Screen name="Posts" component={PostsScreen} />
			<Stack.Screen name="Directs" component={DirectsScreen} />
			<Stack.Screen name="Chat" component={ChatScreen} />
		</Stack.Navigator>
	)
}