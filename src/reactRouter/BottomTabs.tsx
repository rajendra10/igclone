import * as React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'

import {
	HomeScreen,
	SearchScreen,
	IgtvScreen,
	ProfileScreen,
	ActivityScreen,
} from '../screens/index.tsx';


const Tab = createBottomTabNavigator();
export default () => {
	return (
		<Tab.Navigator
			tabBarOptions={{
				activeTintColor: 'red',
				inactiveTintColor: '#4a1c1c',
			}}>
			<Tab.Screen
				name="Home"
				component={HomeScreen}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({color, size}) => (
						<AntDesign
							name="home"
							color={color}
							size={30}
							style={styles.iconStyle}
						/>
					),
				}}
			/>
			{/*<Tab.Screen name="Search" component={SearchScreen} />*/}
			<Tab.Screen
				name="Igtv"
				component={SearchScreen}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({color, size}) => (
						<AntDesign
							name="search1"
							color={color}
							size={30}
							style={styles.iconStyle}
						/>
					),
				}}
			/>
			<Tab.Screen
				name="Activity"
				component={ActivityScreen}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({color, size}) => (
						<AntDesign
							name="hearto"
							color={color}
							size={30}
							style={styles.iconStyle}
						/>
					),
				}}
			/>
			<Tab.Screen
				name="Profile"
				component={ProfileScreen}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({color, size}) => (
						<AntDesign
							name="user"
							color={color}
							size={30}
							style={styles.iconStyle}
						/>
					),
				}}
			/>
		</Tab.Navigator>
	);
};

const styles = StyleSheet.create({
	iconStyle: {
		alignSelf: 'center',
		marginTop: 10,
		flexDirection: 'column',
	},
});