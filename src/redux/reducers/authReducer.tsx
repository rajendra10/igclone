import {AUTH_START, AUTH_SUCCESS, AUTH_FAILURE} from '../actionTypes';

const initialState =  {
	user: undefined,
	isLoading: true,
}

const  authReducer = (state = initialState, action) => {
	switch (action.type) {
		case AUTH_START:
			return {
				...state,
				isLoading: true,
			};
			break;
		case AUTH_SUCCESS : 
			return{
				...state,
				user : action.user,
				isLoading : false
			}
			break;
		case AUTH_FAILURE : 
			return {
				...state, 
				user : null,
				isLoading : false
			}
			break;

		default: return state;
		}
}

export default authReducer;