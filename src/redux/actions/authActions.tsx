import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';

GoogleSignin.configure({
  webClientId: '661986152352-uhjfai9b35lop6m2a2o86e209h1grr79.apps.googleusercontent.com',
});

import {AUTH_START, AUTH_SUCCESS, AUTH_FAILURE} from '../actionTypes'

export const start_auth = ()=> ({type:AUTH_START});
export const auth_success = (user)=> ({type: AUTH_SUCCESS, user});
export const auth_failure = ()=> ({type: AUTH_FAILURE});

//get rnfirebase
export const getUser = ()=>async(dispatch)=>{
    try {
      const userInfo = await GoogleSignin.signInSilently();
      dispatch(auth_success(userInfo));
  } catch (error) {
      dispatch(auth_failure());
  }
}

export const signInWithGoogle = ()=>async(dispatch)=>{
    try{
      const userInfo = await GoogleSignin.signIn();
      dispatch(auth_success(userInfo));
    }catch(e){
      dispatch(auth_failure());
    }
}

export const signOut = ()=>async(dispatch) => {
	  try {
	    await GoogleSignin.revokeAccess();
	    await GoogleSignin.signOut();
	    dispatch(auth_failure()) // Remember to remove the user from your app's state as well
	  } catch (error) {
	    console.error(error);
	  }
}

// export const googleSignIn

