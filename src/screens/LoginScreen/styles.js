import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');

const styles =  StyleSheet.create({
	container:{
		flex : 1,
		paddingBottom: 20,
		alignItems: 'center',
		// flexDirection: 'column-reverse'
		justifyContent:'center',
	},
	imageWrapper:{
		flex: 0.2,
		justifyContent: 'center',
	},
	instagram : {
		height: 60,
		resizeMode:'contain',
	},
	textInputs:{
		flex: 0.2,
		width,
	},
	textInput:{
		padding: 5,
		paddingHorizontal: 20,
		borderWidth: 1,
		borderColor: 'black',
		marginHorizontal: 20,
		marginVertical: 10,
	},
	forgotPassword:{
		width: width,
		textAlign: 'right',
		paddingHorizontal:20,
		color: 'blue',
		fontWeight: '700',
		fontSize:17,


	},
	loginWrapper : {
		width : width-40,
	},
	loginBtn:{
		// marginHorizontal : 20,
		width: width-40,
		paddingVertical: 20,
		borderRadius: 6,
		
	},
	googleBtnWrapper:{

	},
	signupWrapper:{
		flex: 0.1,
		flexDirection: 'row',
		alignItems:'center',
		justifyContent:'center',
	},
	signup :{
		color: 'blue',
		fontWeight: '500',
	},
	footer:{
		flex: 0.2,
		justifyContent: 'flex-end',
		alignItems:'center',
	},
	fb : {
		fontWeight: 'bold',
		fontSize: 26,
		padding:10,
		color: 'pink'
	},
	or :{
		flex: 0.1,
		// backgroundColor: 'pink',
		alignItems: 'center',
		justifyContent: 'center',
	}



})

export default styles;