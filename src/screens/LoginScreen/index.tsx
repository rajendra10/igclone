import React, { useState, useEffect } from 'react';
import { View, Button, Text, TextInput, Image , TouchableOpacity } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {authActions} from '../../redux/actions'
import styles from './styles';
import {GoogleSigninButton} from '@react-native-community/google-signin';


export default ()=>{
	// const navigation = useNavigation();
  const authState = useSelector(store=>store.auth);
  console.log(authState.user);
  const dispatch = useDispatch();
  useEffect(()=>{
    const subscriber = dispatch(authActions.getUser());
    // return subscriber;
  },[])

  if(authState.isLoading) return null;
	return(
		<View style = {styles.container}>

          <View style = {styles.imageWrapper}>
            <Image source = {require('../../assets/instagram.png')}  style = {styles.instagram} />
          </View>
          <View style = {styles.textInputs}>
            <TextInput style = {styles.textInput} placeholder = "Phone Number username or email"/>
            <TextInput style = {styles.textInput} placeholder = "Password"/>
            <Text title = "Forgot Password?" style = {styles.forgotPassword}>Forgot Password ? </Text> 
          </View>


          <View style = {styles.loginWrapper}>
            <Button style = {styles.loginBtn} title = "Log in" />
          </View>

          
          <View style = {styles.or}>
            <Text> OR </Text>
          </View>

          <GoogleSigninButton 
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={()=>dispatch(authActions.signInWithGoogle())}
            disabled={authActions.isMakingCalls} 
          />

          <TouchableOpacity style = {styles.signupWrapper} >
            <Text>Don't have an account </Text>
            <Text style = {styles.signup} > Signup </Text>
          </TouchableOpacity>
          <View style = {styles.footer}>
            <Text> From </Text>
            <Text style = {styles.fb} > Facebook </Text>
          </View>
		</View>
	)
}