import React from 'react';
import {
	View,
	Text,
	TextInput,
	StyleSheet,
	FlatList,
	Image,
	TouchableOpacity,
	Dimensions,
} from 'react-native';

import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';

const {height, width} = Dimensions.get('window');

export default ({style}) => {
	const navigation = useNavigation();

	navigation.setOptions({
		headerShown: true,
		title: 'Directs',
	});
	return (
		<View style={[style, {backgroundColor: 'white', flex: 1}]}>
			<Header {...{navigation}} />
			<FlatList
				ListHeaderComponent={<TopSection  />}
				// data={[...directs, ...directs, ...directs, ...directs]}
				data = {directs}
				renderItem={({item, index}) => <Direct {...{item, index}} />}
				keyExtractor={item => item.id.toString()}
			/>
		</View>
	);
};
const TopSection = () => {
	return (
		<View style={{}}>
			<View
				style={{
					flexDirection: 'row',
					backgroundColor: '#eee',
					marginHorizontal: 40,
					alignItems: 'center',
					height: 40,
					paddingHorizontal: 10,
					borderRadius: 12,
				}}>
				<Ionicons name="search" size={18} style={{paddingHorizontal: 10}} />
				<TextInput placeholder="Search" style={{flex: 1}} />
			</View>
			<Text
				style={{
					fontSize: 22,
					fontWeight: 'bold',
					paddingHorizontal: 10,
					padding: 20,
				}}>
				Messages
			</Text>
		</View>
	);
};
const Header = ({navigation}) => {
	return (
		<View
			style={{
				width,
				flexDirection: 'row',
				height: 50,
				alignItems: 'center',
				paddingHorizontal: 20,
				marginBottom: 20,
				backgroundColor: 'white',
			}}>
			<TouchableOpacity
				onPress = {()=>navigation.goBack()}
			>
				<AntDesign name="arrowleft" size={32} />
			</TouchableOpacity>
			<Text style={{fontSize: 24, fontWeight: '700', flex: 1, marginLeft: 20}}>
				{' '}
				Direct{' '}
			</Text>
			<View
				style={{
					flexDirection: 'row',
					width: 80,
					justifyContent: 'space-between',
				}}>
				<AntDesign name="videocamera" size={32} />
				<Feather name="edit" size={32} />
			</View>
		</View>
	);
};
const Direct = ({item}) => {
	const navigation = useNavigation();
	return (
		<TouchableOpacity
			style={Dstyles.container}
			onPress={() =>
				navigation.push('Shared', {screen: 'Chat', params: {item}})
			}>
			<View style={Dstyles.col1}>
				<Image source={{uri: item.imageUri}} style={Dstyles.profileImage} />
				{item.active && (
					<View
						style={{
							width: 15,
							height: 15,
							borderRadius: 10,
							backgroundColor: 'green',
							position: 'absolute',
							bottom: 10,
							right: 10,
						}}></View>
				)}
			</View>

			<View style={Dstyles.col2}>
				<Text style={Dstyles.name}>{item.userName}</Text>
				<View style={Dstyles.messageWrapper}>
					<Text style={Dstyles.activeMsg}>
						{item.active ? 'Active Now' : `Active ${item.LastActivetime}`}
					</Text>
					<Text style={Dstyles.time}>{item.time}</Text>
				</View>
			</View>
			<AntDesign
				name="camerao"
				size={22}
				color={'gray'}
				style={Dstyles.camerabtn}
			/>
		</TouchableOpacity>
	);
};

const HEIGHT = 70;
const Dstyles = StyleSheet.create({
	container: {
		height: HEIGHT,
		flexDirection: 'row',
		alignItems: 'center',
		// justifyContent:'space-between',
		paddingHorizontal: 20,
		marginVertical: 5,
	},
	col1: {
		height: HEIGHT,
		width: HEIGHT,
		borderRadius: HEIGHT / 2,
	},
	name: {
		fontWeight: '700',
	},
	profileImage: {
		height: HEIGHT - 10,
		width: HEIGHT - 10,
		borderRadius: HEIGHT / 2,
	},
	col2: {
		height: '100%',
		padding: 10,
		flex: 1,
	},
	messageWrapper: {
		flexDirection: 'row',
	},
	activeMsg: {
		color: 'gray',
	},
	camerabtn: {
		flex: 1,
		right: 20,
		position: 'absolute',
	},
});

const directs = [
	{
		id: 1,
		userName: 'AVICII',
		imageUri: 'https://picsum.photos/200/300?u=1',
		active: true,
		lastMessage: 'Idk, ask him.',
		LastActivetime: 'yesterday',
	},

	{
		id: 2,
		userName: 'EVIL',
		imageUri: 'https://picsum.photos/200/300?u=2',
		active: true,
		lastMessage: 'Sent a EVIL Reel',
		LastActivetime: '6h',
	},

	{
		id: 3,
		userName: 'EVIL',
		imageUri: 'https://picsum.photos/200/300?u=2',
		active: false,
		lastMessage: 'Sent a EVIL Reel',
		LastActivetime: '6h',
	},

	{
		id: 4,
		userName: 'DVIL',
		imageUri: 'https://picsum.photos/200/300?u=2',
		active: true,
		lastMessage: 'hey',
		LastActivetime: '7h',
	},
];
