import React from 'react'
import{View, Text} from 'react-native';
import {profile} from '../../mockData';
import {Profile} from '../../components';

export default ()=>{
	return(
		<Profile profile = {profile}/>
	)
}