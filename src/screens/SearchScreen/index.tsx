import React from 'react';
import {View, Text, TouchableOpacity, Button} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PushNotification from "react-native-push-notification";

import {useDispatch} from 'react-redux';
import {authActions} from '../../redux/actions'



export default ()=>{
	const navigation = useNavigation();
	const dispatch = useDispatch();
	return(
		<View>
			<Text> Search Screen</Text>
			<Button 
				onPress = {()=>dispatch(authActions.signOut())}
				title=  "Signout"
			/>
			<Button 
				onPress = {()=>{
					// PushNotification.getScheduledLocalNotifications((itm)=>{
					// 	console.log('notifications', itm);
					// });

					PushNotification.localNotificationSchedule({
					  channelId:'localNotification',
					  message: "My Notification Message", // (required)
					  date: new Date(Date.now() + (5 * 1000)), // in 5 secs
					  
					});
				}}
				title=  "local Notification"
			/>
		</View>
	)
}
