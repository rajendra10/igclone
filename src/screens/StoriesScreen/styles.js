import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	storyContainer:{
		height: height,
		width: width,
		borderRadius: 12,
		overflow: 'hidden',
	},
	header : {
		position: 'absolute' ,
		height: 100,
		width: width,
		borderRadius: 12,
		overflow: 'hidden',
	},
	topBars : {
		paddingHorizontal: 15,
		height: 20,
		width: width,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		position: 'absolute',
		left: 0, 
	},
	topBarBkg:{
		backgroundColor: '#eee',
		height: 2,
		marginRight: 3,
		borderRadius: 3,
		overflow: 'hidden',
	},
	topBarLine :{
		position: 'absolute',
		backgroundColor: 'black',
		height: 2,
		marginRight: 3,
		borderRadius: 3,
	}

})