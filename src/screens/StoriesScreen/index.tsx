import React, {useState, useRef, useEffect, useCallback} from 'react';
import {
	ScrollView,
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	Image,
	FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Animated, {
	useSharedValue,
	useAnimatedStyle,
	withTiming,
	runOnJS,
	interpolate,
	Easing,
} from 'react-native-reanimated';
import {
	TapGestureHandler,
	LongPressGestureHandler,
	State,
} from 'react-native-gesture-handler';
import styles from './styles'
const {height, width} = Dimensions.get('window');
const notchHeight = 88; //TODO FIXIT

import {ProfileCircle} from '../../components';

//TODO update sliderIndex on FlatList Scroll.
const TL = 2000;
export default ({navigation, route}) => {
	const orgIndex = route.params.index;
	const {stories} = route.params;
	const [sliderIndex, setSliderIndex] = useState(orgIndex);
	const flatListRef = useRef();

	const nextStory = () => {
		if (sliderIndex == stories.length - 1) {
			navigation.pop();
			return;
		}
		setSliderIndex(sliderIndex + 1);
	};

	useEffect(() => {
		if (flatListRef.current) {
			flatListRef.current.scrollToOffset({
				offset: sliderIndex * width,
				animated: true,
			});
		}
	}, [sliderIndex]);
	
	return (
		<View style={{flex: 1, backgroundColor: 'black'}}>
			<FlatList
				// onScroll = {()=>onScroll}
				ref={flatListRef}
				data={stories}
				renderItem={({item, index}) => {
					return (
						<Story
							{...{item, index, sliderIndex, nextStory}}
							allstories={stories}
						/>
					);
				}}
				horizontal={true}
				keyExtractor={item => item.user.id.toString()}
				snapToInterval={width}
				disableIntervalMomentum={true}
				showsHorizontalScrollIndicator={false}
			/>
		</View>
	);
};

const Story = ({item, index, sliderIndex, allstories, nextStory}) => {
	const userStories = item.stories;
	const counts = userStories.length;

	const [currentStoryIndex, setCurrentStoryIndex] = useState(0);
	const [timer, setTimer] = useState(TL);
	const progress = useSharedValue(0);
	const paused = useSharedValue(false);

	const handleProgress = (reset) => {
		'worklet';
		if(reset) progress.value = 0;
		progress.value = withTiming(
			TL,
			{duration: TL - progress.value, easing: Easing.linear},
			finished => {
				if (finished) {
					progress.value = 0;
					runOnJS(nextInLine)();
				}
			},
		);
	};

	const nextInLine = (next = true) => {
		if (next && currentStoryIndex == userStories.length - 1) {
			nextStory();
			return;
		}
		progress.value = 0;
		setCurrentStoryIndex(
			next ? currentStoryIndex + 1 : Math.max(currentStoryIndex - 1, 0),
		);
	};

	useEffect(() => {
		progress.value = 0;
		if (index == sliderIndex && !paused.value) {
			handleProgress(true);
		}
	}, [sliderIndex, currentStoryIndex]);

	const onLongPress = ({nativeEvent}) => {
		if (nativeEvent.state === State.BEGAN) {
		} else if (nativeEvent.state === State.ACTIVE) {
			progress.value = progress.value;
		} else {
			handleProgress();
		}
	};

	const onPress = ({nativeEvent}) => {
		if (nativeEvent.state === State.END) {
			if (nativeEvent.absoluteX > width / 2) {
				nextInLine();
			} else {
				nextInLine(0);
			}
		}
	};
	return (
		<LongPressGestureHandler
			onHandlerStateChange={onLongPress}
			minDurationMs={150}>
			<TapGestureHandler onHandlerStateChange={onPress}>
				<View style={styles.storyContainer} >
					<Image
						source={{uri: item.stories[currentStoryIndex].imageUri}}
						style={{flex: 1}}
					/>
					<View style={styles.header}>
						<TopBars {...{counts, currentStoryIndex, paused, progress}} />
						<ProfileCircle
							image={item.user.imageUri}
							name={item.user.name}
							text2={item.stories[currentStoryIndex].postedTime}
							style={{marginTop: 20, paddingHorizontal: 20}}
						/>
					</View>
				</View>
			</TapGestureHandler>
		</LongPressGestureHandler>
	);
};

//TODO remove flikers on currentStoryIndex udates.
const TopBars = ({counts, currentStoryIndex, paused, progress}) => {
	let padding = 30; // paddingHor form styles.js *2
	let marginRight = 3;
	let barWidth = (width - padding - (counts * marginRight)) / counts;

	return (
		<View
			style={styles.topBars}>
			{new Array(counts).fill(0).map((_, i) => {
				const barStyle = useAnimatedStyle(() => {
					if (i < currentStoryIndex) {
						return {left: 0};
					}
					if (i == currentStoryIndex) {
						return {left: interpolate(progress.value, [0, TL], [-barWidth, 0])};
					}
					return {left: -barWidth};
				});
				return (
					<View
						style={[ {width: barWidth}, styles.topBarBkg]}>
						<Animated.View
							style={[{width: barWidth}, styles.topBarLine,barStyle]}
						/>
					</View>
				);
			})}
		</View>
	);
};
