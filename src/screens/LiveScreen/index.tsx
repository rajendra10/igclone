import React, {useState, useRef, useEffect} from 'react';

import {  StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
const {height, width} = Dimensions.get('window')
import { RNCamera } from 'react-native-camera';
import{ ScrollView} from 'react-native-gesture-handler';
import Animated, {useAnimatedGestureHandler, useSharedValue, useAnimatedStyle, interpolate} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import Ionicons from 'react-native-vector-icons/Ionicons'
import styles from './styles';

export default ({navigation})=>{
	
	return (
      <View style={[styles.container]}>
          <Camera />
          <TopOptions/>
          <LeftOptions/>
      </View>
    );
}
const TopOptions = ()=>{
  return(
  <View style = {styles.topOptions} >
    <Ionicons name = "settings" color = "white" size={32} />
    <Ionicons name = "flash-off" color = "white" size={32} />
    <Entypo name = "cross" color = "white" size={32} />
  </View>
    )
}
const LeftOptions = ()=>null;

const Camera = ()=>{
  const tx = useSharedValue(0);
  const takePicture = async function(camera) {
      const options = { quality: 0.5, base64: true };
      const data = await camera.takePictureAsync(options);
      //  eslint-disable-next-line
      console.log(data.uri);
  };
  const onScroll = (e)=>{
    tx.value = e.nativeEvent.contentOffset.x/130;
    // console.log(JSON.stringify(tx.value, 4, 4));
  }

    // const scale = useAnimatedStyle(()=>{
    //   transform : [{scale : interpolate(tx.value, [0, ],
    //       [0.5,1,0.5]
    //   )}]
    // })
    
  return (
    <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        >
          {({ camera, status, recordAudioPermissionStatus }) => {
            if (status !== 'READY') return <PendingView />;
            return(
                <View style = {{position:'absolute', height: 100, width: width, bottom: 50, }}>
                  <ScrollView
                    onScroll = {onScroll}
                    contentContainerStyle = {{height: '100%', }}
                    horizontal
                    snapToInterval = {100+30}
                    disableIntervalMomentum
                  >
                    <View style = {{width: (width/2-37)-30}}/>
                    {new Array(10).fill(0).map((_,i)=>{

                      return(
                        <View style = {{height: 70, width: 70, alignItems:'center', justifyContent:'center', marginHorizontal: 30}}>
                          <Animated.View style = {[styles.capture, {backgroundColor: 'white', height: 60, width: 60} ]} key = {i} >
                          </Animated.View>
                        </View>
                      )
                    })}
                    <View style = {{width: (width/2-37)-30+5}}/>
                  </ScrollView>
                  <TouchableOpacity style = {[styles.capture, {position:'absolute', left: width/2-37,alignSelf:'center', }]}>
                  </TouchableOpacity>
                </View>
            )
            return (
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                {/*<TouchableOpacity onPress={() => takePicture(camera)} style={styles.capture}>
                </TouchableOpacity>*/}

              </View>
            );
          }}
        </RNCamera>
    )
}
                // <TouchableOpacity onPress={() => takePicture(camera)} style={styles.capture}>
                //   <Text style={{ fontSize: 14 }}> SNAP </Text>
                // </TouchableOpacity>


const PendingView = () => (
    <View
      style={styles.pendingView}
    >
      <Text>Waiting</Text>
    </View>
  );