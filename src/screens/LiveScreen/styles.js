import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
	container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
  	height: 70,
  	width: 70,
  	borderRadius: 70,
  	backgroundColor: 'transparent',
  	borderWidth: 4,
  	borderColor: 'white',
  	// margin: 30,
    // flex: 0,
    // backgroundColor: '#fff',
    // borderRadius: 5,
    // padding: 15,
    // paddingHorizontal: 20,
    // alignSelf: 'center',
    // margin: 20,

  },
  pendingView:{
  	flex: 1,
    backgroundColor: 'lightgreen',
    justifyContent: 'center',
    alignItems: 'center',
},
	topOptions:{
		flexDirection: 'row',
		position: 'absolute', 
		height: 60,
		width: width, 
		alignItems: 'center',
		justifyContent:'space-between',
		paddingHorizontal:20,
	}
})