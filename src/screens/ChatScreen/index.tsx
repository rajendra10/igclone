import React from 'react';
import {Text,View, Image} from 'react-native';

export default ({navigation, route})=>{
	const {item} = route.params;
	navigation.setOptions({
		// title : ()=>(<Text>{params.userName}</Text>),
		headerTitle : ()=>(
			<View style = {{flexDirection: 'row', alignItems:'center',}}>
				<View style = {{marginRight: 10,}}>
					<Image source = {{uri: item.imageUri}} style = {{height: 30, width: 30, borderRadius: 30,}}/>
				</View>
				<View>
					<Text style = {{fontSize: 18, fontWeight: '600',}}>{item.userName}</Text>
					<Text>Active Now</Text>
				</View>
			</View>
		),
		headerShown : true,
	})

	return null;
}