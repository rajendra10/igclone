import React from 'react'
import{View, Text} from 'react-native';
import {TapGestureHandler} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated'
export default ()=>{
	return(
		<View>
			<TapGestureHandler onHandlerStateChange = {()=>console.warn('hi')}>
				<Animated.View style = {{padding: 20, backgroundColor: 'blue'}}>
					<Text> ActivityScreen </Text>
				</Animated.View>
			</TapGestureHandler>
		</View>
	)
}