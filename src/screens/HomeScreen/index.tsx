import React, {useRef, useEffect} from 'react';
import {
	View,
	ScrollView,
	Text,
	TouchableOpacity,
	FlatList,
	Image,
	Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
const {height, width} = Dimensions.get('window');
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import {stories, posts} from '../../mockData';

import {Stories, Posts} from '../../components';
import {DirectsScreen, LiveScreen} from '../../screens';


const Tab = createMaterialTopTabNavigator();

export default ()=>{
		  return (
		    <Tab.Navigator
			    initialRouteName= {'Main'}
		    	tabBarOptions={{
				    style: { height: 0},
				  }}
				  swipeVelocityImpact = {0.9}
		    >
		      <Tab.Screen name="Live" component={LiveScreen} />
		      <Tab.Screen name="Main" component={MainView} />
		      <Tab.Screen name="Direct" component={DirectsScreen} />
		    </Tab.Navigator>
		  );
}

const MainView = () => {
	const navigation = useNavigation();
	return (
		<View style={{position: 'relative'}}>
			<Header  />
			<Posts {...{posts}} header={() => <Stories {...{stories}} />} />
		</View>
	);
};





const Header = ({navigateTo}) => {
	const navigation = useNavigation();
	return (
		<View style={[styles.header]}>
			<TouchableOpacity
				style={styles.addBtn}
				onPress={() => navigation.navigate('Live')}>
				<Text style={styles.plus}>+</Text>
			</TouchableOpacity>
			<Image
				style={styles.instagram}
				source={require('../../assets/instagram.png')}
			/>
			<TouchableOpacity onPress={() => navigation.navigate('Direct')}>
				<MaterialCommunityIcons name="telegram" size={32} />
			</TouchableOpacity>
		</View>
	);
};
