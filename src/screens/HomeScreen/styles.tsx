import React from 'react';
import{StyleSheet, Dimensions} from 'react-native';
const {height, width } = Dimensions.get('window');

export default StyleSheet.create({
	header:{
		flexDirection : 'row',
		justifyContent : 'space-between',
		alignItems : 'center',
		paddingHorizontal : 20,
		height: 70,
	},
	instagram : {
		flex: 1,
		height: '60%',
		resizeMode:'center',

	},
	igTitle:{
		fontSize:32,
		fontWeight : '600',
	},
	addBtn:{
		height: 30,
		width: 30,
		borderRadius:10,
		borderColor: 'black',
		borderWidth: 2,
		alignItems:'center',
		justifyContent:'center',
	},
	plus :{
		fontSize:35,
		fontWeight: '500',
		alignSelf:'center',
		textAlign :'center'
	},
	profileImg : {
		flex:1, 
		height: 30,
		width: 30,
		borderRadius: 15, 
		borderColor : 'black',
		borderWidth : 1,
	},
	

})