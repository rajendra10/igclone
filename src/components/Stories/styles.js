import React from 'react';
import {StyleSheet} from 'react-native';

const SIZE = 90;


export default StyleSheet.create({
	flatlist:{
		maxHeight : SIZE,
		paddingHorizontal: 10,

	},
	storyWrapper:{
		height: SIZE,
		width: SIZE,
		justifyContent: 'space-between',
		alignItems:'center',

	},
	image : {
		height: SIZE-20,
		width: SIZE-20,
		borderRadius: SIZE/2,
		borderWidth: 1,
		borderColor: 'brown',
		padding: 1,
	},
	story :{
		height: SIZE-20,
		width: SIZE-20,
		borderRadius: SIZE/2,
		borderWidth: 2,
		borderColor: 'red',
		padding: 1,
		flex: 1,
	},
	by:{
		color: 'black',
	}

})