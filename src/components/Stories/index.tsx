import React from 'react';

import {View, Text, FlatList, Image, TouchableOpacity} from 'react-native';

import styles from "./styles"
import { useNavigation } from '@react-navigation/native';

export default ({stories})=>{
	return(
		<FlatList
			data = {stories}
			renderItem = {({item, index, seperator})=>{
				return <Story {...{item, index}} allstories = {stories} />
			}}
			horizontal = {true}
			keyExtractor = {(item)=>item.user.id.toString()}
			style = {styles.flatlist}
			showsHorizontalScrollIndicator = {false}
		/>
	)
}

const Story = ({item:{user,stories}, index, allstories})=>{
	const navigation = useNavigation();
	return (
		<TouchableOpacity style = {styles.storyWrapper}
			activeOpacity ={0.8}
			onPress = {()=>navigation.navigate('Shared', {screen : 'Stories', params:{stories: allstories, index} })}
		>
			<Image style = {styles.image} source = {{uri: user.imageUri}}/>
			<Text style = {styles.by}>{user.name}</Text>
		</TouchableOpacity>
	)
}