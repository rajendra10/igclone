import React from 'react';
import {View, FlatList, Dimensions, Image, TouchableOpacity} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';

const Tab = createMaterialTopTabNavigator();
const {height, width} = Dimensions.get('window');

export default ({posts}) => {
	return <PostsGridView {...{posts}} />;

	return (
		<Tab.Navigator>
			<Tab.Screen name="Posts" component={Posts} />
			<Tab.Screen name="IGTV" component={IGTV} />
		</Tab.Navigator>
	);
};

const Posts = () => <View></View>;
const IGTV = () => <View></View>;

const PostsGridView = ({posts}) => {
	return (
		<FlatList
			style={{marginTop: 30}}
			data={posts}
			renderItem={({item,index}) => <PostGridView {...{item, posts, index}} />}
			contentContainerStyle = {styles.containerStyle}
			keyExtractor={itm => itm.id.toString()}
			numColumns={3}
			columnWrapperStyle={styles.columnWrapperStyle}
		/>
	);
};
const POSTWIDTH = (width - 60) / 3;
const PostGridView = ({item, posts, index}) => {
	const navigation = useNavigation();
	return (
		<TouchableOpacity style={{width: POSTWIDTH, height: POSTWIDTH + 40}}
			onPress = {()=>navigation.push('Shared', {screen : 'Posts', params:{posts, index} })}
		>
			<Image source={{uri: item.imageUri}} style={styles.postImage} />
		</TouchableOpacity>
	);
};
