import {StyleSheet} from 'react-native';

export default  StyleSheet.create({
	container :{
		flex: 1
	},
	row1 : {
		flexDirection: 'row',
		justifyContent:'space-between',
		alignItems: 'center',
		padding: 10,
	},
	row1Name : {
		fontWeight: 'bold', 
		fontSize: 20,
	},
	row1Icons:{	
		flexDirection: 'row',
		width: 100,
		justifyContent: 'space-around'
	},
	row2 : {
		paddingTop: 20,
	 	flexDirection: 'row',
	 	paddingHorizontal: 10,
	},
	row2Img : {
		height: 90, width: 90,
		borderRadius: 55,
	},
	aggs : {
	 	flexDirection: 'row',
	 	flex: 1,
	 	justifyContent:'space-around',
	 	alignItems:'center',
	},
	agg : {
		alignItems: 'center',
	},
	aggVal : {
		fontWeight: '700',
		fontSize: 20,
		marginBottom: 5,

	},
	aggText : {

	},
	row3 : {
		paddingHorizontal: 10,
		paddingTop: 5,
	},
	row3Name : {
		fontWeight: 'bold',
	}

})