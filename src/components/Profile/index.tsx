import React from 'react';
import {ScrollView, View, Text, Image} from 'react-native';
import styles from './styles';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'

import {ProfileTabsView} from '../'

export default ({profile})=>{
	return (
		<ScrollView style = {styles.container}>
			<View style = {styles.row1}>
				<Text style = {styles.row1Name}>{profile.userName}</Text>
				<View style = {styles.row1Icons}>	
					<AntDesign name = 'bells' size = {22} />
					<Entypo name = 'dots-three-vertical' size = {22} />
				</View>
			</View>
			<View style = {styles.row2} >
				<Image source = {{uri: profile.imageUri}} style = {styles.row2Img} />
				<View style = {styles.aggs}>
					<View style = {styles.agg}>
						<Text style = {styles.aggVal}>{profile.posts}</Text>
						<Text style = {styles.aggText}>Posts</Text>
					</View>
					<View style = {styles.agg}>
						<Text style = {styles.aggVal}>{profile.followers}</Text>
						<Text style = {styles.aggText}>Followers</Text>
					</View>
					<View style = {styles.agg}>
						<Text style = {styles.aggVal} >{profile.followings}</Text>
						<Text style = {styles.aggText} >Followings</Text>
					</View>
				</View>
			</View>
			<View style = {styles.row3} >
				<Text style = {styles.row3Name} > {profile.name}</Text>
				<Text style = {styles.bio} > {profile.bio}</Text>
				<View style = {{flexDirection : 'row',}}>
					<Text> Followed by </Text>
					{profile.followedBy.map(itm=><Text style = {{fontWeight: 'bold', paddingRight:3}}>{itm}</Text>)}
				</View>
			</View>
			<ProfileTabsView posts = {profile.Posts} />
		</ScrollView>
	)
}