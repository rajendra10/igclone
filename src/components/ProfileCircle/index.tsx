import React from 'react';

import {View, Text, Image} from 'react-native';
import styles from './styles';
export default ({image, name, style= {}, imageStyle = {}, text2 })=>{
	return(
		<View style = {[styles.profileWrapper, style]}>
			<Image source= {{uri : image}} style = {[styles.profileImage, imageStyle]}/>
				<Text style = {styles.profileName}>{name}</Text>
				<Text style = {styles.profileName}>{text2}</Text>
		</View> 
	)
}