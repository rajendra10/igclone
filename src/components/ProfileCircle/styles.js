import {StyleSheet} from 'react-native'

export default StyleSheet.create({
	profileWrapper : {
		flexDirection: 'row',
		alignItems: 'center',
	},
	profileImage :{
		width: 30,
		height: 30,
		borderRadius: 15,
		marginRight: 5,

	},
	profileName : {
		fontWeight: '700',
		marginRight: 10,
	}
})