import React, {useRef, useEffect} from 'react'
import {View, Text , Image, FlatList, Dimensions } from 'react-native'
import {Post} from '../';

const{height, width} = Dimensions.get('window');

const ITEM_HEIGHT = (height/1.5)+20+30 //const TODO 

export default (params)=>{
	const {posts, index = 0} = params.route ? params.route.params : params;
	if(params.navigation){
		console.log(params.navigation);
		params.navigation.setOptions({
			headerShown : true,
			title: posts[0].userName,
			//TODO 
		})
	}
	const ref = useRef();
	useEffect(()=>{
		// ref.current.scrollToIndex({index, animated: false,viewPosition : 0});
	}, []);
	return(
		<View style = {{height}}>
		<FlatList
			initialNumToRender= {1}
			initialScrollIndex = {index}
			ListHeaderComponent = {params.header ? params.header() : null}
			ref ={ref}
			data = {posts}
			renderItem={({item, index, seperator}) => {
				return <Post {...{item, index}} />
			}}
			keyExtractor={(item) => item.id.toString()}
			showsVerticalScrollIndicator={false}
			 getItemLayout={(data, index) => (
			    {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
			  )}
		/>
		</View>
	)
}