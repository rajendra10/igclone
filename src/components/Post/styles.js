import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window')

export default StyleSheet.create({
	postContainer:{
		width, height: height/1.5,
	},
	postHead : {
		paddingHorizontal : 10,
		height : 30,
	},
	postBody :{
		marginTop:3,
		flex: 1,
	},
	postImg : {
		flex: 1, 

	},
	postBottom :{
		paddingHorizontal : 10,
	},
	igPostIcon :{
		fontSize: 30,
		padding : 5,
	},
	postIcons:{
		flexDirection : 'row',
		justifyContent : 'space-between',	
	},
	body:{
		padding: 10,
		flexDirection: 'row',
	},
	likeHeartContainer : {
		...StyleSheet.absoluteFillObject,
		flex: 1,
		alignItems:'center',
		justifyContent: 'center',

	},
})