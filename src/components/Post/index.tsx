import React, {useState, useRef, useEffect} from 'react';
import {View , Image, Text, TouchableOpacity} from 'react-native';


import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Animated, {useSharedValue, useAnimatedStyle, withTiming,withSpring, withRepeat, withSequence, useAnimatedGestureHandler, runOnJS } from 'react-native-reanimated'
import {TapGestureHandler, } from 'react-native-gesture-handler';

import {ProfileCircle} from '../';
import styles from './styles';

export default ({item, index}) => {
	const [clipBody, setClipBody] = useState(true);
	const likedSV = useSharedValue(0);
	const [liked,setLiked ] = useState(false);
	const [likes, setLikes] = useState(item.likes)
	//TODO update liked button state.
	useEffect(()=>{
		setLikes(likes + (liked ? 1 : -1)) ;
	},[liked])

	return (
		<View style={{marginBottom: 20}}>
			<View style={styles.postContainer}>
				<View style={styles.postHead}>
					<ProfileCircle name={item.userName} image={item.profileUri} />
				</View>

				<View style={styles.postBody}>
					<Image style={styles.postImg} source={{uri: item.imageUri}} />
					<LikeHeart {...{liked, setLiked, likedSV}} />
				</View>

				<View style={styles.postBottom}>
					<View style={styles.postIcons}>
						<View style={{flexDirection: 'row'}}>
							{liked ? (
								<AntDesign name= "heart" style={styles.igPostIcon} color="red" />
							) : (
								<AntDesign
									name="hearto"
									style={styles.igPostIcon}
									color="black"
								/>
							)}

							<EvilIcons name="comment" style={styles.igPostIcon} />
							<MaterialCommunityIcons
								name="telegram"
								style={styles.igPostIcon}
							/>
						</View>
						<View style={{flexDirection: 'row'}}>
							<AntDesign name="save" style={styles.igPostIcon} />
						</View>
					</View>
					<View style={styles.postLikes}>
						<Text>{likes} likes</Text>
					</View>
				</View>
			</View>

			<TouchableOpacity
				style={styles.body}
				onPress={() => setClipBody(!clipBody)}>
				<Text style={{fontWeight: '700', paddingRight: 3}}>
					{item.userName}
				</Text>
				<Text numberOfLines={clipBody ? 1 : 0} style={{flex: 1}}>
					{item.body}
				</Text>
			</TouchableOpacity>
		</View>
	);
};

//TODO double taps rn-gesture-handler
const LikeHeart = ({likedSV, liked, setLiked}) => {
	const animatedStyles = useAnimatedStyle(() => {
	    return {
	      transform: [{scale: likedSV.value }]
	    };
	  });

	const eventHandler = useAnimatedGestureHandler({
	  onStart: (event, ctx) => {
	    // pressed.value = true;
	    // likedSV.value = false;
	  },
	  onEnd: (event, ctx) => {
	  	if(!liked) runOnJS(setLiked)(true);
	    likedSV.value  = withSequence(withSpring(1), withTiming(0))
	  },
	},[]);

	return (
		<TapGestureHandler
			onGestureEvent = {eventHandler}
			numberOfTaps={2}>
			<Animated.View
				style={[styles.likeHeartContainer]}
			>
			<Animated.View style = {animatedStyles} >
				<AntDesign name="heart" size={100} color="white" />
			</Animated.View>
			</Animated.View>
		</TapGestureHandler>
	);
};
